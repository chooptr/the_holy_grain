﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

	[SerializeField] private GameObject uGreenBar;

	[SerializeField] private GameObject uYellowBar;

	[SerializeField] private GameObject uRedBar;

	[SerializeField] private AActor uAActor;

	private const float LERP_TIME = 1f;

	// Use this for initialization
	public void Start() {
		uAActor.Health.Pairwise().Subscribe(UpdateGreenBar);
	}

	private void UpdateYellowBar(Pair<float> values) {
		float t = 0f, currentLerpTime = 0f;
		uYellowBar.LateUpdateAsObservable()
				  .TakeWhile(_ => currentLerpTime < LERP_TIME + Time.deltaTime)
				  .Subscribe(_ => {
								 float f = Mathf.Lerp(values.Previous, values.Current, t / LERP_TIME);
								 uYellowBar.GetComponent<Image>().fillAmount = f / 100f;
								 currentLerpTime += Time.deltaTime;
								 t = currentLerpTime / LERP_TIME;
								 t = Mathf.Sin(t * Mathf.PI * 0.5f);
							 });
	}

	private void UpdateGreenBar(Pair<float> values) {
		float t = 0f, currentLerpTime = 0f;
		this.LateUpdateAsObservable()
			.TakeWhile(_ => currentLerpTime < LERP_TIME + 2 * Time.deltaTime)
			.DoOnCompleted(() => UpdateYellowBar(values))
			.Subscribe(_ => {
						   float f = Mathf.Lerp(values.Previous, values.Current, t / LERP_TIME);
						   uGreenBar.GetComponent<Image>().fillAmount = f / 100f;
						   currentLerpTime += 2 * Time.deltaTime;
						   t = currentLerpTime / LERP_TIME;
						   t = Mathf.Sin(t * Mathf.PI * 0.5f);
					   });
	}

	public void LateUpdate() {
		transform.LookAt(Camera.main.transform);
		transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward);
	}

}