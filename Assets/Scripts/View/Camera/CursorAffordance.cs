﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class CursorAffordance : MonoBehaviour {

	[SerializeField] private Texture2D uWalkCursor;

	[SerializeField] private Texture2D uNopeCursor;

	[SerializeField] private Texture2D uAttackCursor;

	[SerializeField] private Texture2D uQuestCursor;

	[SerializeField] private Texture2D uUnknownCursor;

	[SerializeField] Vector2 uCursorHotspot = new Vector2(0, 0);

	private CameraRaycaster uCameraRaycaster;

	// Use this for initialization
	void Start() {
		uCameraRaycaster = GetComponent<CameraRaycaster>();
		uCameraRaycaster.LayerHit.Subscribe(x => {
												Debug.Log(x);
												OnLayerChange(x);
											});

	}

	private void OnLayerChange(ELayer eLayer) {
		switch (eLayer)
		{
			case ELayer.Unwalkable:
				Cursor.SetCursor(uNopeCursor, uCursorHotspot, CursorMode.Auto);
				break;
			case ELayer.Enemy:
				Cursor.SetCursor(uAttackCursor, uCursorHotspot, CursorMode.Auto);
				break;
			case ELayer.Walkable:
				Cursor.SetCursor(uWalkCursor, uCursorHotspot, CursorMode.Auto);
				break;
			case ELayer.RaycastEndStop:
				Cursor.SetCursor(uQuestCursor, uCursorHotspot, CursorMode.Auto);
				break;
			default:
				Cursor.SetCursor(uUnknownCursor, uCursorHotspot, CursorMode.Auto);
				break;
		}
    }

}