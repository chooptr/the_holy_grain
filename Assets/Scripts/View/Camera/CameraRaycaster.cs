﻿using UniRx;
using UnityEngine;

public class CameraRaycaster : MonoBehaviour {

	private RaycastHit uHit;

	public RaycastHit Hit {
		get { return uHit; }
	}

	private ReactiveProperty<ELayer> uLayerHit = new ReactiveProperty<ELayer>();

	public ReactiveProperty<ELayer> LayerHit {
		get { return uLayerHit; }
	}

	[SerializeField] private float uDistanceToBackground = 100f;

	private Camera uViewCamera;

	// Use this for initialization
	private void Start() {
		uViewCamera = Camera.main;
	}

	// Update is called once per frame
	private void Update() {
		RaycastHit? hit = RaycastForLayer();
		if (hit.HasValue) {
			uHit            = hit.Value;
			uLayerHit.Value = (ELayer) hit.Value.transform.gameObject.layer;
			return;
		}

		uHit.distance   = uDistanceToBackground;
		uLayerHit.Value = ELayer.RaycastEndStop;
	}

	private RaycastHit? RaycastForLayer() {
		Ray ray = uViewCamera.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		bool hasHit = Physics.Raycast(ray, out hit, uDistanceToBackground);
		if (hasHit) return hit;
		return null;
	}

}