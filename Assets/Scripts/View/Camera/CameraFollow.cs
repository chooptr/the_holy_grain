﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	private GameObject uPlayer;

	// Use this for initialization
	void Start () {
		uPlayer  = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void LateUpdate () {
		transform.position = uPlayer.transform.position;
	}
}
