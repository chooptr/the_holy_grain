﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.CrossPlatformInput;
using UniRx;

[RequireComponent(typeof(ThirdPersonCharacter))]
public class PlayerMovement : MonoBehaviour {

	[SerializeField] private float uBrakeRadius = 0.8f;

	[SerializeField] private float uAttackBrakeRadius = 3f;

	private ThirdPersonCharacter uCharacter;

	private CameraRaycaster uCameraRaycaster;

	private Vector3 uCurrentClickTarget, uClickPoint;

	private bool uIsInDirectMode = false;

	private FloatReactiveProperty uHeatlh = new FloatReactiveProperty(100f);

	public FloatReactiveProperty Health {
		get { return uHeatlh; }
	}

	// Use this for initialization
	void Start() {
		uCameraRaycaster    = Camera.main.GetComponent<CameraRaycaster>();
		uCharacter          = GetComponent<ThirdPersonCharacter>();
		uCurrentClickTarget = transform.position;
	}

	// Update is called once per frame
	void FixedUpdate() {
		if (Input.GetKeyDown(KeyCode.G)) {
			uIsInDirectMode     = !uIsInDirectMode;
			uCurrentClickTarget = transform.position;
		}

		if (uIsInDirectMode) ProcessDirectMovement();
		else ProcessMouseMovement();
	}

	private void ProcessDirectMovement() {
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");
		Vector3 camForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
		Vector3 move = v * camForward + h * Camera.main.transform.right;

		uCharacter.Move(move, false, false);
	}

	private void ProcessMouseMovement() {
		if (Input.GetMouseButton(0)) {
			uClickPoint = uCameraRaycaster.Hit.point;
			switch (uCameraRaycaster.LayerHit.Value) {
				case ELayer.Walkable:
					uCurrentClickTarget = ShortenDestination(uClickPoint, uBrakeRadius);
					break;
				case ELayer.Enemy:
					uCurrentClickTarget = ShortenDestination(uClickPoint, uAttackBrakeRadius);
					break;
			}
		}
		WalkToDestination();
	}

	private void WalkToDestination() {
		if ((uCurrentClickTarget - transform.position).magnitude >= uBrakeRadius)
			uCharacter.Move(uCurrentClickTarget - transform.position, false, false);
		else {
			uCharacter.Move(Vector3.zero, false, false);
		}
	}

	private Vector3 ShortenDestination(Vector3 destination, float shortening) {
		Vector3 reductionVector = (destination - transform.position).normalized * shortening;
		return destination - reductionVector;
	}

	public void OnDrawGizmos() {
		Gizmos.color = Color.blue;
		Gizmos.DrawLine(transform.position, uCurrentClickTarget);
		Gizmos.DrawSphere(uCurrentClickTarget, 0.05f);
		Gizmos.DrawSphere(uClickPoint, 0.1f);
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, uAttackBrakeRadius);
	}

}