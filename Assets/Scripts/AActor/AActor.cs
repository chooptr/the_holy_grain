﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class AActor : MonoBehaviour {

	[SerializeField] protected float uMaxHealth = 100f;

	protected ReactiveProperty<float> uHealth;

	public ReactiveProperty<float> Health {
		get { return uHealth; }
	}

	public void Awake() {
		uHealth = new ReactiveProperty<float>(uMaxHealth);
	}

}