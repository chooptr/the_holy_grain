﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class AEnemy : AActor {

	[SerializeField] private float uAttackTriggerRadius = 4f;

	private APlayer uPlayer;

	public void Start() {
		uPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<APlayer>();
		uPlayer.transform.ObserveEveryValueChanged(x => x).Subscribe()
	}

}