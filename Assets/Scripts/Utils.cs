﻿public enum ELayer {

	Walkable = 8,
	Unwalkable = 9,
	Enemy = 10,
	RaycastEndStop = -1

}